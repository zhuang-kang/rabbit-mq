package com.zhuang.springboot_rabbitmq.service.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Classname FanoutDuanXinConsumer
 * @Description FanoutDuanXinConsumer
 * @Date 2021/4/17 15:06
 * @Created by dell
 */
@Component
@RabbitListener(queues = {"duanxin.fanout.queue"})
public class FanoutDuanXinConsumer {

    @RabbitHandler
    public void receiveMessage(String message){
        System.out.println("dunaxin fanout --- 接受到了订单消息是：->"+ message);
    }
}
