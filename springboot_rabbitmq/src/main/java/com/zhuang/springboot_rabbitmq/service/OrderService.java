package com.zhuang.springboot_rabbitmq.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @Classname OrderService
 * @Description 业务层
 * @Date 2021/4/17 14:46
 * @Created by dell
 */
@Service
public class OrderService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    //模拟用户下单
    public void makeOrder(String userid, String productid, int num) {
        //根据商品id查询库存是否充足
        String orderId = UUID.randomUUID().toString();
        //保存订单
        System.out.println("订单生产成功" + orderId);

        //通过MQ来完成消息的分发
        String exchangeName = "fanout_order_exchange";
        String routingKey = "";
        rabbitTemplate.convertAndSend(exchangeName, routingKey, orderId);
    }

}
